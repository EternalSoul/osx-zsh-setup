# Dev env setup

Set up dev environment with ZSH, Oh My Zsh and goodies, Pyenv, Poetry

**Base `.zshrc` file (from repo) can be used if you install all steps listed below (excluding iTerm2 which is MacOS specific)**

---

## iTerm2 (OS X only)

```
$ brew cask install iterm2
```
    
Get the iTerm color settings

- [Solarized Dark theme](https://raw.githubusercontent.com/mbadolato/iTerm2-Color-Schemes/master/schemes/Solarized%20Dark%20-%20Patched.itermcolors) (patched version to fix the bright black value)
- [Solarized Light theme](https://raw.githubusercontent.com/altercation/solarized/master/iterm2-colors-solarized/Solarized%20Light.itermcolors)
- [More themes @ iterm2colorschemes](http://iterm2colorschemes.com/)
    
Just save it somewhere and open the file(s). The color settings will be imported into iTerm2. Apply them in iTerm through iTerm → preferences → profiles → colors → load presets. You can create a different profile other than `Default` if you wish to do so.

---

## ZSH

### MacOS

```
$ brew install zsh
```

### Arch

```
$ sudo pacman -Syu zsh
```

### Debian/Ubuntu

```
$ sudo apt install zsh
```

---

## Oh My Zsh 

More info here: https://github.com/robbyrussell/oh-my-zsh

### Install with curl

```    
$ sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

### Install zsh-syntax-highlighting
```
$ git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

### Install zsh-autosuggestions
```
$ git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```

### Install Powerlevel10k
```
$ git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```

Update `~/.zshrc` with `ZSH_THEME="powerlevel10k/powerlevel10k"`

---

## Update plugins settings

In `~/.zshrc` add `plugins = (git zsh-autosuggestions zsh-syntax-highlighting)`

**Note (iTerm2)**
  
If the `zsh-autosuggestions` do not appear to show, 
it could be a problem with your color scheme. 
Under "iTerm → Preferences → Colors tab", check the value of Black Bright, 
that is the color your auto suggestions will have. 
It will be displayed on top of the Background color. 
If there is not enough contrast between the two, 
you won't see the suggestions even if they're actually there.

---

## Pyenv

### MacOS

```
$ brew install pyenv
```

### Arch

```
$ sudo pacman -Syu base-devel
$ sudo pacman -Syu pyenv
```

`base-devel` needed for installing python with pyenv

### Debian/Ubuntu

```
$ sudo apt install pyenv
```

### Enable pyenv in zsh

Add `eval "$(pyenv init -)"` to `.zshrc`

---

## Poetry

```
$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

Add `export PATH="$HOME/.poetry/bin:$PATH"` to `.zshrc`

To create virtualenvs inside project folder execute `poetry config virtualenvs.in-project true`

### To create new virtualenv with Poetry

```
$ pyenv local 3.7.5
$ poetry new project_name
```

Try running the script outside your virtual environment. This won't work.

```
$ python project_name/example.py
```

Run the script within your virtual environment, using the 'run'-command.

```
$ poetry run python project_name/example.py
```

Spawn a shell within your virtual environment.
```
$ poetry shell
```

Try running the script again, after having spawned the shell within your virtual environment.

```
$ python project_name/example.py
```

---

## iTerm2 options

### Enable word jumps and word deletion, aka natural text selection (iTerm)

By default, word jumps (option + → or ←) and word deletions (option + backspace) do not work. 
To enable these, go to `"iTerm → Preferences → Profiles → Keys → Load Preset... → Natural Text Editing → Boom! Head explodes"`

### Custom prompt styles

By default, your prompt will now show “user@hostname” in the prompt. 
This will make your prompt rather bloated. 
Optionally set `DEFAULT_USER` in `~/.zshrc` to your regular username 
(these must match) to hide the “user@hostname” info when you’re logged in 
as yourself on your local machine. 
You can get your exact username value by executing `whoami` in the terminal.

For further customisation of your prompt, you can follow a great guide here: https://code.tutsplus.com/tutorials/how-to-customize-your-command-prompt--net-24083

### Install a patched font (iTerm2)

- [Meslo](https://github.com/powerline/fonts/blob/master/Meslo%20Slashed/Meslo%20LG%20M%20Regular%20for%20Powerline.ttf) (the one in the screenshot). Click "view raw" to download the font.
- [Source Code Pro](https://github.com/powerline/fonts/blob/master/SourceCodePro/Source%20Code%20Pro%20for%20Powerline.otf) has better alignment for the glyphs @14px.
- [Others @ powerline fonts](https://github.com/powerline/fonts)
    
Open the downloaded font and press "Install Font".

Set this font in iTerm2 (14px is my personal preference) (iTerm → Preferences → Profiles → Text → Change Font).

Restart iTerm2 for all changes to take effect.